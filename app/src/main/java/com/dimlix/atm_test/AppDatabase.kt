package com.dimlix.atm_test

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dimlix.atm_test.atm.model.Atm
import com.dimlix.atm_test.atm.repository.AtmDao

@Database(entities = arrayOf(Atm::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun atmDao(): AtmDao

    companion object {
        private var instance: AppDatabase? = null

        private val DBNAME: String = "ATM_DB"

        @Synchronized
        fun getInstance(context: Context, inMemory: Boolean): AppDatabase {
            // Allow in-memory DB to make test easier
            if (instance == null || !instance?.isOpen!!) {
                val databaseBuilder = if (inMemory) {
                    Room.inMemoryDatabaseBuilder(context.applicationContext, AppDatabase::class.java)
                            .allowMainThreadQueries()
                } else {
                    Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DBNAME)
                }
                instance = databaseBuilder.build()
            }
            return instance as AppDatabase
        }

        @Synchronized
        fun getInstance(context: Context): AppDatabase = getInstance(context, false)
    }
}
