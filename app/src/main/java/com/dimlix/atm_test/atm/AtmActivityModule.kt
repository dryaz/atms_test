package com.dimlix.atm_test.atm

import com.dimlix.atm_test.atm.screen.AtmMapViewModelFactory
import com.dimlix.atm_test.atm.usecase.AtmUseCase
import dagger.Module
import dagger.Provides

@Module
class AtmActivityModule {
    @Provides
    internal fun provideAtmViewModelFactory(atmUseCase: AtmUseCase): AtmMapViewModelFactory {
        return AtmMapViewModelFactory(atmUseCase)
    }
}
