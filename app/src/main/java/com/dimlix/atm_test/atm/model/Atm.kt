package com.dimlix.atm_test.atm.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

@Entity(indices = arrayOf(Index("latitude"), Index("longitude"),
        Index("name"), Index("addressLine")))
data class Atm(
        @PrimaryKey
        val id: String,
        val name: String,
        val latitude: Double,
        val longitude: Double,
        val city: String,
        val zip: String,
        val addressLine: String) : ClusterItem {

    @Ignore
    private var _latLng: LatLng? = null

    override fun getSnippet(): String = addressLine

    override fun getTitle(): String = name

    override fun getPosition(): LatLng {
        _latLng?.let {
            return it
        } ?: let {
            _latLng = LatLng(it.latitude, it.longitude)
            return _latLng!!
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        return (id == (other as Atm).id)
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}