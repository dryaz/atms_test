package com.dimlix.atm_test.atm.model.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AtmResponse(
        @SerializedName("name")
        @Expose
        val name: String,
        @SerializedName("latitude")
        @Expose
        val latitude: String,
        @SerializedName("longitude")
        @Expose
        val longitude: String,
        @SerializedName("sonectId")
        @Expose
        val sonectId: String,
        @SerializedName("address")
        @Expose
        val address: Address) {

    data class Address(
            @SerializedName("city")
            @Expose
            val city: String,
            @SerializedName("zip")
            @Expose
            val zip: String,
            @SerializedName("formatted")
            @Expose
            val formatted: String)
}