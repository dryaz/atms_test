package com.dimlix.atm_test.atm.repository

import com.dimlix.atm_test.atm.model.network.AtmResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface AtmApi {
    @GET("/data/ATM_20181005_DEV.json")
    fun getAllAtms(): Observable<Response<List<AtmResponse>>>
}