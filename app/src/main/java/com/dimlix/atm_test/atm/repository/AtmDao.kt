package com.dimlix.atm_test.atm.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dimlix.atm_test.atm.model.Atm
import io.reactivex.Flowable

@Dao
interface AtmDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(atm: Atm): Long

    @Query("SELECT * FROM Atm")
    fun getAllAtms(): Flowable<List<Atm>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAtms(items: List<Atm>)
}
