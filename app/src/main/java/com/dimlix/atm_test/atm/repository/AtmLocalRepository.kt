package com.dimlix.atm_test.atm.repository

import com.dimlix.atm_test.atm.model.Atm
import io.reactivex.Observable

class AtmLocalRepository(val atmDao: AtmDao) : AtmRepository {
    override fun saveAtms(atms: List<Atm>) = atmDao.saveAtms(atms)
    override fun getAtmList(): Observable<List<Atm>> = atmDao.getAllAtms().toObservable()
}