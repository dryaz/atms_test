package com.dimlix.atm_test.atm.repository

import android.accounts.NetworkErrorException
import com.dimlix.atm_test.atm.model.Atm
import io.reactivex.Observable
import javax.inject.Inject

class AtmRemoteRepository @Inject constructor(val atmApi: AtmApi) : AtmRepository {
    override fun saveAtms(atms: List<Atm>) {
        TODO("not implemented")
    }

    override fun getAtmList(): Observable<List<Atm>> = atmApi.getAllAtms()
            .map { if (it.isSuccessful) it.body()!! else throw NetworkErrorException() }
            .flatMapIterable { it }
            .map {
                Atm(id = it.sonectId, name = it.name, addressLine = it.address.formatted,
                        city = it.address.city, zip = it.address.zip,
                        latitude = it.latitude.toDouble(), longitude = it.longitude.toDouble())
            }.toList()
            .toObservable()

}
