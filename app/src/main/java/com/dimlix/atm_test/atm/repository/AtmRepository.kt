package com.dimlix.atm_test.atm.repository

import androidx.annotation.StringDef
import com.dimlix.atm_test.atm.model.Atm
import io.reactivex.Observable


interface AtmRepository {
    @StringDef(LOCAL, REMOTE)
    annotation class SourceType

    companion object {
        const val LOCAL = "local"
        const val REMOTE = "remote"
    }

    fun getAtmList(): Observable<List<Atm>>

    fun saveAtms(atms: List<Atm>)
}