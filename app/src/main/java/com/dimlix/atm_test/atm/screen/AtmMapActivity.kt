package com.dimlix.atm_test.atm.screen

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.*
import com.dimlix.atm_test.R
import com.dimlix.atm_test.atm.model.Atm
import com.dimlix.atm_test.atm.screen.list.AtmListAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.clustering.ClusterManager
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class AtmMapActivity : AppCompatActivity() {

    companion object {
        // Zoom level when user choose item
        private const val ITEM_ZOOM = 16f
    }

    @Inject
    lateinit var _atmViewModelFactory: AtmMapViewModelFactory
    private lateinit var _atmViewModel: AtmMapViewModel

    private val _rxPermission = RxPermissions(this)
    private lateinit var _map: GoogleMap

    private var _enableLocationButton: MenuItem? = null
    private lateinit var _clusterManager: ClusterManager<Atm>

    private val _data: MutableList<Atm> = ArrayList()
    private val _adapter: AtmListAdapter = AtmListAdapter(_data)
    private val _snapHelper: SnapHelper = PagerSnapHelper()
    private val _linearLayoutManager = LinearLayoutManager(this,
            LinearLayoutManager.HORIZONTAL, false)

    private lateinit var _fusedLocationClient: FusedLocationProviderClient

    private val _scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            when (newState) {
                RecyclerView.SCROLL_STATE_IDLE -> {
                    val snappedView = _snapHelper.findSnapView(_linearLayoutManager)
                    snappedView?.let {
                        val pos = _linearLayoutManager.getPosition(it)
                        val atm = _data[pos]
                        _map.animateCamera(CameraUpdateFactory.newLatLngZoom(atm.position, ITEM_ZOOM))
                    }
                }
            }
            super.onScrollStateChanged(recyclerView, newState)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        _atmViewModel = ViewModelProviders.of(this,
                _atmViewModelFactory).get(AtmMapViewModel::class.java)

        _atmViewModel.errorLiveData.observe(this, Observer {
            showNetworkError(it)
        })

        _fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        initGMap()
    }

    private fun initGMap() {
        val googleMap = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        googleMap.getMapAsync { map ->
            _map = map
            _clusterManager = ClusterManager(this, map)
            _clusterManager.setAnimation(true)

            _clusterManager.setOnClusterClickListener {
                _map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        it.position, Math.floor((_map.cameraPosition.zoom + 3).toDouble()).toFloat()),
                        300, null)
                true
            }

            _clusterManager.setOnClusterItemClickListener {
                for (i in 0 until _data.size) {
                    if (it == _data[i]) {
                        listAtm.scrollToPosition(i)
                        _map.animateCamera(CameraUpdateFactory.newLatLngZoom(it.position, ITEM_ZOOM))
                    }
                }
                true
            }

            map.setOnCameraIdleListener(_clusterManager)
            map.setOnMarkerClickListener(_clusterManager)
            map.uiSettings.isMapToolbarEnabled = false
            enableUsersLocation()
            _atmViewModel.atmsLiveData.observe(this, Observer {
                showAtms(it)
            })
            initList()
            _atmViewModel.loadAtms()
        }
    }

    private fun initList() {
        listAtm.addOnScrollListener(_scrollListener)
        listAtm.layoutManager = _linearLayoutManager
        _snapHelper.attachToRecyclerView(listAtm)

        listAtm.adapter = _adapter
        listAtm.itemAnimator?.let {
            if (it is SimpleItemAnimator) {
                it.supportsChangeAnimations = false
            }
        }
    }

    private fun showAtms(atms: List<Atm>) {
        // TODO don't update the same pins in future
        _clusterManager.clearItems()
        _clusterManager.addItems(atms)
        _data.clear()
        _data.addAll(atms)
        _adapter.notifyItemRangeChanged(0, atms.size)
        atms[0].let {
            _map.animateCamera(CameraUpdateFactory.newLatLngZoom(it.position, ITEM_ZOOM))
        }
    }

    @SuppressLint("RxLeakedSubscription", "RxSubscribeOnError", "CheckResult", "MissingPermission")
    private fun enableUsersLocation() {
        _rxPermission
                .requestEachCombined(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe { permission ->
                    if (permission.granted) {
                        _map.isMyLocationEnabled = true
                        updateLastLocation()
                    } else if (permission.shouldShowRequestPermissionRationale) {
                        showMyLocationPermissionRetryDialog()
                    } else {
                        showMyLocationPermissionSettingsDialog()
                    }
                    _enableLocationButton?.isVisible = !permission.granted
                }
    }

    @SuppressLint("MissingPermission")
    private fun updateLastLocation() {
        _fusedLocationClient.lastLocation
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful && task.result != null) {
                        _adapter.updateUsersCoordinates(task.result?.latitude, task.result?.longitude)
                    } else {
                        Log.w("!@#", "updateLastLocation:exception", task.exception)
                    }
                }
    }

    private fun showNetworkError(error: String?) {
        findViewById<View>(R.id.map)?.let {
            val msg = if (error != null) error else getString(R.string.network_error)
            val snackbar = Snackbar
                    .make(it, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry) { _atmViewModel.updateAtmsFromRemote() }
            snackbar.show()
        }
    }

    private fun showMyLocationPermissionSettingsDialog() {
        findViewById<View>(R.id.map)?.let {
            val snackbar = Snackbar
                    .make(it, R.string.need_enable_location_settings, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.go_to_settings) {
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri
                        startActivity(intent)
                    }
            snackbar.show()
        }
    }

    private fun showMyLocationPermissionRetryDialog() {
        findViewById<View>(R.id.map)?.let {
            val snackbar = Snackbar
                    .make(it, R.string.need_enable_location, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry) { enableUsersLocation() }
            snackbar.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_map, menu)
        _enableLocationButton = menu?.findItem(R.id.action_enable_location)
        _enableLocationButton?.isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                R.id.action_enable_location -> {
                    enableUsersLocation()
                    return true
                }
            }
            return super.onOptionsItemSelected(item)
        } ?: let {
            return super.onOptionsItemSelected(item)
        }
    }

}
