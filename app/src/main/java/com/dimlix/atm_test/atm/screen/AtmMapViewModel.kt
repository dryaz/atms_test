package com.dimlix.atm_test.atm.screen

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dimlix.atm_test.atm.model.Atm
import com.dimlix.atm_test.atm.usecase.AtmUseCase
import io.reactivex.android.schedulers.AndroidSchedulers

class AtmMapViewModel(val atmUseCase: AtmUseCase) : ViewModel() {

    val errorLiveData: MutableLiveData<String> = MutableLiveData()
    val atmsLiveData: MutableLiveData<List<Atm>> = MutableLiveData()
    val updateAtmsResultLiveData: MutableLiveData<Boolean> = MutableLiveData()

    @SuppressLint("RxLeakedSubscription", "RxSubscribeOnError", "CheckResult")
    fun loadAtms() {
        atmUseCase.getAllLocalAtms()
                .filter{it.isNotEmpty()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(atmsLiveData::postValue)
        updateAtmsFromRemote()
    }

    @SuppressLint("RxLeakedSubscription", "RxSubscribeOnError", "CheckResult")
    fun updateAtmsFromRemote() {
        atmUseCase.getAtmsFromServerAndRefreshLocal().subscribe(
                {
                    updateAtmsResultLiveData.postValue(true)
                },
                {
                    errorLiveData.postValue(it.localizedMessage)
                }
        )
    }
}