package com.dimlix.atm_test.atm.screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dimlix.atm_test.atm.usecase.AtmUseCase

class AtmMapViewModelFactory(val atmUseCase: AtmUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AtmMapViewModel::class.java)) {
            return AtmMapViewModel(atmUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}