package com.dimlix.atm_test.atm.screen.list

import android.annotation.SuppressLint
import android.location.Location
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.dimlix.atm_test.R
import com.dimlix.atm_test.atm.model.Atm
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.item_atm.view.*

class AtmListAdapter(private val atms: MutableList<Atm>) : RecyclerView.Adapter<AtmListAdapter.ViewHolder>() {

    var _usersPosition: LatLng? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_atm, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(atms[position])
    }

    override fun getItemCount(): Int {
        return atms.size
    }

    fun updateUsersCoordinates(latitude: Double?, longitude: Double?) {
        if (latitude != null && longitude != null) {
            _usersPosition = LatLng(latitude, longitude)
        }
        notifyItemRangeChanged(0, atms.size)
    }

    inner class ViewHolder(val parent: View) : RecyclerView.ViewHolder(parent) {
        val title = parent.tvTitle
        val distance = parent.tvDistance
        val address1 = parent.tvAdress1
        val address2 = parent.tvAdress2

        @SuppressLint("SetTextI18n")
        fun bind(atm: Atm) {
            title.text = atm.title
            address1.text = "${atm.zip}, ${atm.city}"
            address2.text = atm.addressLine
            _usersPosition?.let {
                val array = FloatArray(3)
                Location.distanceBetween(it.latitude, it.longitude, atm.latitude, atm.longitude, array)
                val meters = array[0]
                if (meters > 1000F) {
                    distance.text = distance.context.getString(R.string.distance_kmeters_value,
                            meters / 1000f)
                } else {
                    distance.text = distance.context.getString(R.string.distance_meters_value, meters)
                }
            } ?: run {
                distance.setText(R.string.computing)
            }

            itemView.setOnClickListener {
                with(itemView.context) {
                    Toast.makeText(this, this.getString(R.string.welcome_to, atm.name),
                            Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
