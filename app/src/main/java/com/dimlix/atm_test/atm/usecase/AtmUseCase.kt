package com.dimlix.atm_test.atm.usecase

import com.dimlix.atm_test.atm.repository.AtmRepository
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named

class AtmUseCase @Inject constructor(@Named(AtmRepository.REMOTE) private val atmRemoteRepository: AtmRepository,
                                     @Named(AtmRepository.LOCAL) private val atmLocalRepository: AtmRepository) {

    fun getAllLocalAtms() = atmLocalRepository.getAtmList()
            .subscribeOn(Schedulers.io())

    fun getAtmsFromServerAndRefreshLocal() = atmRemoteRepository.getAtmList()
            .doOnNext(atmLocalRepository::saveAtms)
            .subscribeOn(Schedulers.io())
}
