package com.dimlix.atm_test.di

import android.content.Context
import com.dimlix.atm_test.App
import com.dimlix.atm_test.AppDatabase
import com.dimlix.atm_test.atm.repository.*
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    internal fun provideAtmDao(context: Context): AtmDao {
        return AppDatabase.getInstance(context).atmDao()
    }

    @Singleton
    @Provides
    @Named(AtmRepository.LOCAL)
    internal fun provideLocalAtmRepository(atmDao: AtmDao): AtmRepository {
        return AtmLocalRepository(atmDao)
    }
}
