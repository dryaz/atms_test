package com.dimlix.atm_test.di

import com.dimlix.atm_test.atm.AtmActivityModule
import com.dimlix.atm_test.atm.screen.AtmMapActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {
    @ContributesAndroidInjector(modules = arrayOf(AtmActivityModule::class))
    internal abstract fun bindAtmMapAcitivity(): AtmMapActivity
}
