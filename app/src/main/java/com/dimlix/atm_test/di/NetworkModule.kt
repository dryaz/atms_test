package com.dimlix.atm_test.di

import com.dimlix.atm_test.atm.repository.AtmApi
import com.dimlix.atm_test.atm.repository.AtmRemoteRepository
import com.dimlix.atm_test.atm.repository.AtmRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    private val BASE_URL: String = "http://207.154.210.145:8080/"

    @Provides
    fun provideRetrofit(gson: Gson): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(OkHttpClient())
                .build()
    }

    @Provides
    @Singleton
    fun provideAtmApi(restAdapter: Retrofit): AtmApi {
        return restAdapter.create(AtmApi::class.java)
    }

    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Singleton
    @Provides
    @Named(AtmRepository.REMOTE)
    internal fun provideRemoteAtmRepository(atmApi: AtmApi): AtmRepository {
        return AtmRemoteRepository(atmApi)
    }
}
